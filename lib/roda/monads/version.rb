# frozen_string_literal: true

class Roda
  module Monads
    VERSION = '0.2.0'
  end
end
